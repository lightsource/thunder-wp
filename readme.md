# Thunder WP

## What is it?

Framework of a WordPress theme that makes modular development easy and hassle-free. The heart here is
the [modular blocks](https://gitlab.com/lightsource/front-blocks)
that easy to create and [turn into Gutenberg components](https://gitlab.com/lightsource/acf-block) using
the [ACF blocks feature](https://www.advancedcustomfields.com/resources/blocks/).

The framework includes the following packages:

* [Container](https://github.com/PHP-DI/PHP-DI)
* [Logger](https://gitlab.com/lightsource/log)
* [ACF Blocks](https://gitlab.com/lightsource/acf-blocks) (based
  on [Front Blocks](https://gitlab.com/lightsource/front-blocks))
* [ACF Groups](https://gitlab.com/lightsource/acf-groups)
* [Data Types](https://gitlab.com/lightsource/data-types) (based
  on [Laravel validation](https://github.com/illuminate/validation))

You can see an example of usage in the [WP Theme Bones](https://gitlab.com/lightsource/wp-theme-bones/)
repository, which is a starter theme that uses the framework.

## How to use

1. Install the composer package

`composer require lightsource/thunder-wp`

2. Make a ThunderWP instance and call the `loadFromFile()` method

```php
use LightSource\ThunderWP\ThunderWP;

require_once __DIR__ . '/vendor/autoload.php';

$thunderWP = new ThunderWP();
$thunderWP->loadFromFile(__DIR__ . '/thunder-wp.php');
```

3. Create the config file

```php
<?php

use DI\Container;
use LightSource\DataTypes\Interfaces\DataTypesInterface;
use LightSource\ThunderWP\{AcfBlockAssets, DataTypes, Images, Templates, Theme, WooTemplates};
use LightSource\ThunderWP\Factories\{AcfBlocksFactory, AcfGroupsFactory, FrontBlocksFactory, LoggerFactory};
use LightSource\ThunderWP\Interfaces\{AcfBlockAssetsInterface,
    TemplatesInterface,
    ThemeInterface,
    ImagesInterface,
    WooTemplatesInterface
};
use LightSource\ThunderWP\Interfaces\Factories\{AcfBlocksFactoryInterface,
    AcfGroupsFactoryInterface,
    FrontBlocksFactoryInterface,
    LoggerFactoryInterface
};

$container = new Container();

return [
    'thunder' => [
        'container' => $container,
        // Instances of these classes will be created automatically (with the $container->get())
        // If implements Hooks interface, then the 'setHooks()' method will be called automatically
        // If implements ModuleInterface, then it'll be loaded automatically from the config
        // If implements FactoryInterface, then an instance will be gotten and saved into the container
        'instances' => [
           LoggerFactory::class,
           FrontBlocksFactory::class,
           AcfGroupsFactory::class,
           AcfBlocksFactory::class,
           // if key is set, then the couple will be saved into the container
           AcfBlockAssetsInterface::class => AcfBlockAssets::class,
           ImagesInterface::class => Images::class,
           DataTypesInterface::class => DataTypes::class,
           ThemeInterface::class => Theme::class,
           TemplatesInterface::class => Templates::class,
           WooTemplatesInterface::class => WooTemplates::class,
           FrontClear::class,
           // your classes here
        ],
    ],
    'logger' => [
        'folder' => __DIR__ . '/logs',
         // optionally. implements LoggerNotificationSettingsInterface
         'notificationSettings' => LoggerNotificationSettings::class,
    ],
    'frontBlocks' => [
        'paths' => [
            [
                 'namespace' => 'your\blocks\namespace\here',
                 'folder' => __DIR__ . '/src/Blocks',
            ],
        ],
    ],
    'acfGroups' => [
        'paths' => [
            [
                'namespace' => 'your\groups\namespace\here',
                'folder' => __DIR__ . '/src/AcfGroups',
                'phpFileRegex' => '/.php$/',
            ],
            [
                'namespace' => 'your\blocks\namespace\here',
                'folder' => __DIR__ . '/src/Blocks',
                'phpFileRegex' => '/Data\.php$/',
            ],
        ],
    ],
    'acfBlocks' => [
        'pathToBlockPages' => __DIR__ . '/assets/block-pages',
        'urlOfBlockPages' => get_stylesheet_directory_uri() . '/assets/block-pages',
        'phpFileRegex' => '/(?<!Data)\.php$/',
    ],
    'acfBlockAssets' => [
        // implements AcfBlockAssetsSettingsInterface
        'settings' => Settings::class,
        // implements AcfBlockAssetsPageInterface
        'page' => Page::class,
    ],
    'images' => [
        'folder' => __DIR__ . '/assets/images',
        'excludedSizes' => [
            '1536x1536',
            '2048x2048',
            'medium_large',
            'woocommerce_thumbnail',
            'woocommerce_single',
            'woocommerce_gallery_thumbnail',
        ],
    ],
    'dataTypes' => [
        // removes slashes added by WP
        'isStripSlashes' => true,
    ],
    'theme' => [
        // Declare to WP features that the theme supports
        'supports' => [
            'menus',
        ],
        // disables the native WP feature
        'isWithoutComments' => true,
        // ACF options pages
        'optionPages' => [
            [
                'page_title' => 'Site Settings',
                'menu_title' => 'Site Settings',
                'menu_slug' => 'site-settings',
                'capability' => 'edit_posts',
                'redirect' => false,
            ],
        ],
        // HTML preloading.
        // link[rel=preload] will be added for these fonts for every page
        'fontsToPreload' => [
            get_stylesheet_directory_uri() . '/assets/fonts/signika/light.woff2',
            get_stylesheet_directory_uri() . '/assets/fonts/signika/medium.woff2',
            get_stylesheet_directory_uri() . '/assets/fonts/signika/bold.woff2',
        ],
    ],
    'templates' => [
        // Project CSS/JS defaults (like fonts).
        // Defined block's CSS/JS will be added to all pages
        'defaultsBlock' => Defaults::class,

        // Controls current header/footer post. Must implement TemplatesPageInterface
        'page' => TemplatesPage::class,

        // Page templates that you need. They will appear in the WP template dropdown
        'custom' => [
            'custom-privacy-page' => 'Privacy Page',
        ],

        // Relate templates and block classes.
        // Note: The classes must implement the 'TemplateBlockInterface' interface
        // Alternatively, you can define a callable value
        // You can use any default WP theme templates, without the extension, like 'home', 'archive', '404' etc
        'supported' => [
            'page' => ContentThemeClassic::class,
            'custom-privacy-page' => PrivacyThemeClassic::class,
            'single-product' => function () {
                wp_redirect(get_site_url());
                exit;
            },
        ],
    ],
    'wooTemplates' => [
        // Relate Woo templates and block classes.
        // Note: The classes must implement the 'WooBlockInterface' interface
        // Alternatively, you can define a callable value
        'supported' => [
            'checkout/form-checkout.php' => CheckoutThemeClassic::class,
        ],
    ],
    'frontCleaner' => [
         // disable unnecessary things on the front
         'excluded' => [  
            'wpBlocksStyles',
            'wpEmojiStyles',
            'wpDuotone',
            'softwareVersionsInHead',
            'wooStyles',
            'wooAssetsOnUnrelatedPages',
            'contactForm7Styles',
            'contactForm7AssetsOnUnrelatedPages',
        ],
    ],
];
```

Then

1. Create [Acf Blocks](https://gitlab.com/lightsource/acf-blocks) in the `src/Blocks` folder  
   You can define any arguments in the constructor and  `loadByAcf($pageId,...)` method. They'll be resolved by the
   container.
2. Create [Acf Groups](https://gitlab.com/lightsource/acf-groups) in the `src/ACFGroups` folder.  
   You can also create Acf Groups in the `src/Blocks` folder, but only with the `Data` end, e.g. `LinkData.php`. It's
   used to optimize loading of the both packages.
3. Enjoy

## Child theme mode

The child theme mode is supported, but with one limitation:

* child theme styles WILL NOT appear on the parent's templates (because the parent has its own header's HTML)

It may be suitable for most cases, as allows to 'split' the styles logically, and the templates still work properly. So
if there is no 'overridden' template, then the parent's one will be used.

To improve the performance of the child templates, you can add
a [mu-plugin](https://gitlab.com/lightsource/strangler-by-thunderwp) which will 'disable' the parent's theme and
unnecessary plugins for specific requests only.

## Framework's Modules

Except Factories for the Blocks/Groups packages, it also contains:

1. `Theme`

* Setups theme supports
* Logs about mail errors
* Preloads fonts
* Signups ACF option pages
* Creates header/footer html (`getHtmlHeader()` and `getHtmlFooter()`)
* Disables bloatware on the front, like WP blocks' CSS and Emoji, Woo styles on irrelevant pages...

2. `DataTypes`

* Extends [Data Types](https://gitlab.com/lightsource/data-types)
* Applies `stripslashes_deep` to the $data, to compensate the default WP behavior about `$_POST`

3. `AcfBlockAssets`

* Helper for the AcfBlocks package
* Allows to define resource's version
* Allows to dynamically add page's CSS
* Contains a fix for the WooCommerce checkout page (changes the name of asset's file, as checkout and thank-you pages
  have the same url, but may have different assets)

4. `Images`

* Allows to get inline SVG (for color changes; smart, so uses href for second and next times)
* Removes extra sizes
* WebpExpress plugin support (see the `Advanced usage` below)

5. `Templates`

* Allows to 'mock' WordPress theme and page templates, so you don't need to create files physically
* Takes care of loading and rendering of the chosen block
* Takes care of header/footer

6. `WooTemplates`

* Allows to 'mock' WooCommerce templates, so you don't need to create files physically
* Takes care of loading and rendering of the chosen block

7. `WPObjects/Cpt`

* Provides an easy way to signup CPTs

8. `WPObjects/Taxonomy`

* Provides an easy way to signup taxonomies

9. `WpOjbects/Menus`

* Provides an easy way to get menus data

10. `FrontCleaner`

* Disabled unused WP/plugin's assets (CSS/JS) on the front

## Advanced usage

### 1. Header/Footer

Optionally. The recommended way for header/footer is to create posts for each, and add the related block inside the post
as a
Gutenberg block (with ACF fields as settings, as usually).

Then implement the `ViewInterface` and define this class in the config instances. The framework will automatically
render these posts for all the defined templates (see the `templates` option in
the config), before and after the template block.

Also, you don't need to worry about the HTML head, like `<!DOCTYPE html><html>...`, it'll be added automatically too.

View class:

```php
use LightSource\ThunderWP\Interfaces\TemplatesPageInterface;

class View implements TemplatesPageInterface {
    public function getHeaderPostId(): int{
        // todo get from SiteSettings or wherever
    }

    public function getFooterPostId(): int{
       // todo get from SiteSettings or wherever
    }
}
```

Config:

```php
return [
    'templates' => [
        'view' => View::class,
    ],
];

```

### 2. Factories

If you've classes, which instances can't be created by the container automatically (e.g. contain string constructor
argument or require a custom setup), you can create a factory for that class, with implementing the `FactoryInterface`.

```php
use LightSource\ThunderWP\Interfaces\Factories\FactoryInterface;

class MyFactory implements FactoryInterface {
    public function makeInstance()
    {
       $instance =  new YourClass('beta');
       $instance->setHooks();
       
       return $instance;
    }
    
     public function getContainerProperties($instance): array{
        // optionally
        return [
            YourClassInterface::class => $instance,
        ];
    }
}
```

Then pass this factory into the `factories` array.

Config file:

```php
'thunder' => [
    'factories' => [
       MyFactory::class,
    ],
],
```

The framework will automatically call `makeInstance()` for all the factories, and save response of
the `getContainerProperties()` into the container.

### 3. Images

Image tag will be created by the `Image` class, so in Twig templates you won't be able to add class.
It can be done in the PHP call, but it isn't the best practice, as in Twig you won't see the class. That's why the
framework adds th custom Twig function, `_image`, you can call it for the image to add class in the Twig in the obvious
way.

```
{{ _image(image,"block__image") }}
```

### 4. CPTs and Taxonomies

You can signup CPTs easily, using the Cpt class.

1. Create a class that extends the `Cpt` or `Taxonomy` class

CPT:

```php
<?php

use LightSource\ThunderWP\Cpt;

class Books extends Cpt
{
    const SLUG = 'books';

    public function __construct()
    {
        parent::__construct();

        $this->setName('Books');
        $this->setSingularName('Book');
        // restrict, but only if it needs
        $this->setIsForAdminsOnly(true);
        $this->setColumns([
            'date' => '',
            'email' => 'Email',
        ]);
        $this->setSortableColumns([
            'email' => 'Email',
        ]);
        $this->setCustomSettings([
            'description' => 'Best books',
            'menu_icon' => 'dashicons-tag',
        ]);
    }

    protected function modifyColumnOrderQuery(string $columnSlug, WP_Query $query): void
    {
        parent::modifyColumnOrderQuery($columnSlug, $query);

        // todo
        switch ($columnSlug) {
            case 'email':
                // $query->set('meta_key', MyGroup::getAcfFieldName(MyGroup::FIELD_EMAIL));
                // $query->set('orderby', 'meta_value');
                break;
        }
    }

    protected function modifySearchQuery(string $searchValue, WP_Query $query): void
    {
        parent::modifySearchQuery($searchValue, $query);

        // todo
        // required, otherwise will ignore meta (because AND in mysql)
        /*$query->set('s', ''); 
        $query->set('meta_query', [
            [
                'relation' => 'OR',
                [
                    'key' => MyGroup::getAcfFieldName(MyGroup::FIELD_EMAIL),
                    'value' => $searchValue,
                ],
            ]
        ]);*/
    }

    public function printColumn(string $columnSlug, int $postId): void
    {
        parent::printColumn($columnSlug, $postId);

        switch ($columnSlug) {
            case 'email':
                // todo
                break;
        }
    }
}
```

Taxonomy:

```php
<?php

use LightSource\ThunderWP\WpObjects\Taxonomy;

class Authors extends Taxonomy
{
    const SLUG = 'authors';

    public function __construct()
    {
        parent::__construct();

        $this->setName('Authors');
        $this->setSingularName('Author');
        // restrict, but only if it needs
        $this->setIsForAdminsOnly(true);
        $this->setTargetPostTypes([
            Books::SLUG,
        ]);
    }
}
```

2. In the config, define these classes within the `hooks` argument

```php
return [
    'thunder'=>[
        'hooks' => [
          Books::class,
         Authors::class,
        ],
    ],
]
```

## 5. Webp support

If you want to have automatic Webp conversion then:

1. Install and activate the [WebpExpress plugin](https://wordpress.org/plugins/webp-express/)
2. Configure the plugin to:

- Operation mode: `CDN friendly`
- Create webp files upon request?: `false`
- Convert on upload: `true`
- Bulk convert: press and convert if you already have images
- Alter HTML: `false`

Then press `Save settings and force new .htaccess rules`

3. That's it. The framework automatically adds filters to images to support WebpExpress. (When the plugin is enabled)

By default, the WebpExpress plugin alters HTML, so it doesn't listen to hooks, like `wp_get_attachment_image_src` and
makes 'dirty' replace, with parsing HTML on `the_content` hook. It takes unnecessary resources.

So, we've disabled the `alterHTML` option and the framework
will listen to `wp_get_attachment_image_src` and `convertImageSrcsetToWebp` hooks and
will call Webp plugin's method to get the .webp url. It's a 'clean' way to do the job.

Images will be converting to webp automatically during uploading new ones to the library. Enjoy.

### 6. Menus

#### 6.1) Information

You can easily get Menu Items (up to 3 levels) using the `Menus` class.
First of all, you need to define the implementation classes of the Menu interfaces in the config:

(use the default classes from the framework or provide yours)

```php
return [
    'thunder' => [
        'instances' => [
             MenuDataInterface::class => MenuData::class,
             MenuItemInterface::class => MenuItem::class,
             MenusInterface::class => Menus::class,
        ],
    ],
];
```

Then just get `MenusInterface` instance using the container and call `->getMenu('my-id')` where it needs.

### 6.2) Twig arguments

In addition to the above, there is the `->getMenuTwigArguments()` method, that allows to get arguments directly for your
twig
template.
You need

1. to implement `MenuItemBlockInterface` in your blocks (links) that you want to see as menuItems

```php
class MyLink extends AcfBlock implements MenuItemBlockInterface {
    public function loadByMenuItem(MenuItemInterface $menuItem): void{
        $this->load();
        // todo
    }
}
```

2. pass their instances into the method

```php
class Menu extends AcfBlock {
    protected MyLink $myLink;
    protected array $menusData;
    
    public function __construct(private MenusInterface $menus) {
        parent::__constructor();
    }
    
    public function loadByMenu(string $slug):void{
        $this->load();
        
        // it supports 3 levels, each level can have a unique block
        $this->menusData = $this->menus->getMenuTwigArguments($slug, MyLink::class, MyLink::class, MyLink::class);
    }
}
```

3. add similar to the Menu's twig

```twig
{% for menu in menusData %}
    <div class="menu__item{% if menu.isActive or menu.isChildActive %} menu__item--active{% endif %}">
        {{ _include(menu.item, 'menu__link') }}

        {% if menu.children %}
            <div class="menu__sub-items">
                {% for child in menu.children %}
                    {{ _include(child.item, 'menu__sub-link') }}
                {% endfor %}
            </div>
        {% endif %}
    </div>
{% endfor %}
```

That's it. Enjoy

### 7. Plugins & related packages

**Required plugins (necessary for the framework):**

* [ACF PRO](https://www.advancedcustomfields.com/)

**Recommended packages:**

* [Front Block Assets](https://gitlab.com/lightsource/front-block-assets) - special package for Front Blocks
  for
  hassle-free compiling of `.scss` and `.ts` assets (based on LaravelMix)
* [Thunder WP JS](https://gitlab.com/lightsource/thunder-wp-js) - set of JS files to make development easier

You can see an example of usage of these packages in
the [WP Theme Bones](https://gitlab.com/lightsource/wp-theme-bones/)
repository, which is a starter theme that uses the framework.

**Recommended (start pack) plugins:**

Images

* [Webp Express](https://wordpress.org/plugins/webp-express/)
* [Save Svg](https://wordpress.org/plugins/safe-svg/)

Security

* [WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/)
* [Disable REST API](https://wordpress.org/plugins/disable-json-api/)
* [UpdraftPlus](https://wordpress.org/plugins/updraftplus/)

Forms

* [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
* [Contact Form 7: Flamingo](https://wordpress.org/plugins/flamingo/)
* [Contact Form 7: Recaptcha](https://wordpress.org/plugins/wpcf7-recaptcha/)
* [WP Mail SMTP](https://wordpress.org/plugins/wp-mail-smtp/)

SEO

* [Yoast](https://wordpress.org/plugins/wordpress-seo/)
* [Yoast: ACF](https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/)
* [Redirection](https://wordpress.org/plugins/redirection/)
* [IndexNow](https://wordpress.org/plugins/indexnow/)
* [WP Super Cache](https://wordpress.org/plugins/wp-super-cache/)

Others

* [Admin Menu Groups](https://wordpress.org/plugins/admin-menu-groups/)
* [Yoast: Duplicate post](https://wordpress.org/plugins/duplicate-post/)
* [Woo Commerce](https://wordpress.org/plugins/woocommerce/)

### 8. Default config

Below is just a default config, without any descriptions. Can be used for a new project:

```php
<?php

use DI\Container;
use LightSource\DataTypes\Interfaces\DataTypesInterface;
use LightSource\ThunderWP\{AcfBlockAssets,
    DataTypes,
    FrontCleaner,
    Images,
    Interfaces\WpObjects\MenuDataInterface,
    Interfaces\WpObjects\MenuItemInterface,
    Interfaces\WpObjects\MenusInterface,
    Templates,
    Theme,
    WooTemplates,
    WpObjects\MenuData,
    WpObjects\MenuItem,
    WpObjects\Menus
};
use LightSource\ThunderWP\Factories\{AcfBlocksFactory, AcfGroupsFactory, FrontBlocksFactory, LoggerFactory};
use LightSource\ThunderWP\Interfaces\ImagesInterface;

$container = new Container();

return [
    'thunder' => [
        'container' => $container,
        'instances' => [
            LoggerFactory::class,
            FrontBlocksFactory::class,
            AcfGroupsFactory::class,
            AcfBlocksFactory::class,
            AcfBlockAssets::class,
            MenuDataInterface::class => MenuData::class,
            MenuItemInterface::class => MenuItem::class,
            MenusInterface::class => Menus::class,
            ImagesInterface::class => Images::class,
            DataTypesInterface::class => DataTypes::class,
            Theme::class,
            Templates::class,
            WooTemplates::class,
            FrontCleaner::class,
            // todo your classes here
        ],
    ],
    'logger' => [
        'folder' => __DIR__ . '/logs',
        // todo
        'notificationSettings' => LoggerNotificationSettings::class,
    ],
    'frontBlocks' => [
        // todo 
        'paths' => [
            [
                'namespace' => 'your\blocks\namespace\here',
                 'folder' => __DIR__ . '/src/Blocks',
            ],
        ],
    ],
    'acfGroups' => [
        'paths' => [
            [
                // todo
                'namespace' => 'your\groups\namespace\here',
                'folder' => __DIR__ . '/src/AcfGroups',
                'phpFileRegex' => '/.php$/',
            ],
            [
                // todo
                'namespace' => 'your\blocks\namespace\here',
                'folder' => __DIR__ . '/src/Blocks',
                'phpFileRegex' => '/Data\.php$/',
            ],
        ],
    ],
    'acfBlocks' => [
        'pathToBlockPages' => __DIR__ . '/assets/block-pages',
        'urlOfBlockPages' => get_stylesheet_directory_uri() . '/assets/block-pages',
        'phpFileRegex' => '/(?<!Data)\.php$/',
    ],
    'acfBlockAssets' => [
        // todo
        'settings' => Settings::class,
        'page' => Page::class,
    ],
    'images' => [
        'folder' => __DIR__ . '/assets/images',
        'excludedSizes' => [
            '1536x1536',
            '2048x2048',
            'medium_large',
            'woocommerce_thumbnail',
            'woocommerce_single',
            'woocommerce_gallery_thumbnail',
        ],
    ],
    'dataTypes' => [
        'isStripSlashes' => true,
    ],
    'theme' => [
        'supports' => [
            'menus',
            'woocommerce',
        ],
        // todo
        'optionPages' => [
            [
                'page_title' => 'Site Settings',
                'menu_title' => 'Site Settings',
                'menu_slug' => 'site-settings',
                'capability' => 'edit_posts',
                'redirect' => false,
            ],
        ],
        // todo
        'fontsToPreload' => [
            get_stylesheet_directory_uri() . '/assets/fonts/signika/light.woff2',
            get_stylesheet_directory_uri() . '/assets/fonts/signika/medium.woff2',
            get_stylesheet_directory_uri() . '/assets/fonts/signika/bold.woff2',
        ],
    ],
    'templates' => [
        // todo
        'defaultsBlock' => Defaults::class,
        // todo
        'page' => TemplatesPage::class,
        'custom' => [],
        // todo
        'supported' => [
            'page' => ContentThemeClassic::class,
        ],
    ],
    'wooTemplates' => [
        'supported' => [],
    ],
    'frontCleaner' => [
         // todo
         'wp' => [  
            'blocksStyles',
            'emojiStyles',
            'duotone',
            'versionInHead',
        ],
        'yoast' => [
            'versionInHead',
        ],
        'wooCommerce' => [
             'styles',
             'assetsOnUnrelatedPages',
        ],
        'contactForm7' => [
            'styles',
            'assetsOnUnrelatedPages',
            'autoTop',
        ], 
    ],
];

```