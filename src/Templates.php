<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use DI\Container;
use Exception;
use LightSource\FrontBlocks\Interfaces\CreatorInterface as FrontBlocksCreatorInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;
use LightSource\ThunderWP\Interfaces\TemplateBlockInterface;
use LightSource\ThunderWP\Interfaces\TemplatesInterface;
use LightSource\ThunderWP\Interfaces\ThemeInterface;
use LightSource\ThunderWP\Interfaces\TemplatesPageInterface;
use Psr\Log\LoggerInterface;
use WP_Post;
use WP_Theme;

class Templates extends CommonTemplates implements TemplatesInterface
{
    private ?TemplatesPageInterface $page;
    private ThemeInterface $theme;
    private array $supported;
    private array $custom;
    private string $defaultsBlock;
    private Container $container;

    public function __construct(
        FrontBlocksCreatorInterface $frontBlocksCreator,
        LoggerInterface $logger,
        RendererInterface $renderer,
        ThemeInterface $theme,
        Container $container
    ) {
        parent::__construct($frontBlocksCreator, $logger, $renderer);

        $this->page = null;
        $this->theme = $theme;
        $this->supported = [];
        $this->custom = [];
        $this->defaultsBlock = '';
        $this->container = $container;
    }

    protected function getPage(): ?TemplatesPageInterface
    {
        return $this->page;
    }

    protected function getTheme(): ThemeInterface
    {
        return $this->theme;
    }

    protected function getWpTemplatesData(): array
    {
        return [
            'embed' => 'is_embed',
            '404' => 'is_404',
            'search' => 'is_search',
            'front_page' => 'is_front_page',
            'home' => 'is_home',
            'privacy_policy' => 'is_privacy_policy',
            'post_type_archive' => 'is_post_type_archive',
            'tax' => 'is_tax',
            'attachment' => 'is_attachment',
            'single' => 'is_single',
            'page' => 'is_page',
            'singular' => 'is_singular',
            'category' => 'is_category',
            'tag' => 'is_tag',
            'author' => 'is_author',
            'date' => 'is_date',
            'archive' => 'is_archive',
        ];
    }

    protected function getCurrentPageTemplate(): string
    {
        $currentPost = get_post(get_queried_object_id());
        if ($currentPost) {
            $pageTemplate = get_post_meta($currentPost->ID, '_wp_page_template', true);
            if ($pageTemplate &&
                key_exists($pageTemplate, $this->custom) &&
                key_exists($pageTemplate, $this->supported)) {
                return $pageTemplate;
            }
        }

        return '';
    }

    protected function getCurrentSingleTemplate(): string
    {
        foreach ($this->supported as $supportedTemplate => $block) {
            // numbers, e.g. '404' converted by PHP into int
            $supportedTemplate = (string)$supportedTemplate;

            // only single templates
            if (false === strpos($supportedTemplate, 'single-')) {
                continue;
            }

            $cptName = str_replace('single-', '', $supportedTemplate);

            if (!is_singular([$cptName,])) {
                continue;
            }

            return $supportedTemplate;
        }

        return '';
    }

    protected function getCurrentWpTemplate(): string
    {
        $wpTemplatesData = $this->getWpTemplatesData();

        foreach ($this->supported as $supportedTemplate => $block) {
            // numbers, e.g. '404' converted by PHP into int
            $supportedTemplate = (string)$supportedTemplate;

            // skip Page and Single templates
            if (key_exists($supportedTemplate, $this->custom) ||
                false !== strpos($supportedTemplate, 'single-')) {
                continue;
            }

            $checkFunction = $wpTemplatesData[$supportedTemplate] ?? '';
            $isTemplateInUse = is_callable($checkFunction) ?
                call_user_func($checkFunction) :
                false;

            if (!$isTemplateInUse) {
                continue;
            }

            return $supportedTemplate;
        }

        return '';
    }

    protected function getCurrentTemplate(): string
    {
        $template = $this->getCurrentPageTemplate();
        $template = $template ?: $this->getCurrentSingleTemplate();

        return $template ?: $this->getCurrentWpTemplate();
    }

    protected function printPostContent(int $postId): void
    {
        global $post;

        // setup
        $post = get_post($postId);
        setup_postdata($postId);

        // render
        the_content();

        // back to the origin
        wp_reset_postdata();
    }

    protected function printHeader(): void
    {
        $headerPostId = $this->page->getHeaderPostId();

        if (!$headerPostId) {
            get_header();
            return;
        }

        $this->theme->printHtmlHeader();
        $this->printPostContent($headerPostId);
    }

    protected function printFooter(): void
    {
        $footerPostId = $this->page->getFooterPostId();

        if (!$footerPostId) {
            get_footer();
            return;
        }

        $this->printPostContent($footerPostId);

        $this->theme->printHtmlFooter();
    }

    protected function enqueueDefaultsBlock(): void
    {
        if (!$this->defaultsBlock) {
            return;
        }

        try {
            $blockInstance = $this->getFrontBlocksCreator()->create($this->defaultsBlock);
        } catch (Exception $exception) {
            $this->getLogger()->error('DefaultsBlock from the config is wrong', [
                'defaultsBlock' => $this->defaultsBlock,
                'error' => $exception->getMessage(),
            ]);

            return;
        }

        // fake render, without printing, as we need only to 'enqueue' its styles/scripts
        $this->getRenderer()->render($blockInstance);
    }

    public function getConfigName(): string
    {
        return 'templates';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->supported = $configArguments['supported'] ?? [];
        $this->custom = $configArguments['custom'] ?? [];
        $this->defaultsBlock = $configArguments['defaultsBlock'] ?? '';

        if (isset($configArguments['page'])) {
            $this->page = $this->container->get($configArguments['page']);
        }
    }

    public function setHooks(): void
    {
        add_action('template_redirect', [$this, 'replaceWPTemplateIfCurrentTemplateIsSupported'], 1);

        add_filter('template_include', [$this, 'maybePrintPage',]);
        add_filter('theme_templates', [$this, 'addPageTemplates'], 10, 4);
    }

    public function replaceWPTemplateIfCurrentTemplateIsSupported(): void
    {
        $currentTemplate = $this->getCurrentTemplate();

        if (!key_exists($currentTemplate, $this->supported)) {
            return;
        }

        $wpTemplatesData = $this->getWpTemplatesData();
        foreach ($wpTemplatesData as $templateName => $isTemplateFunction) {
            // 'assign' to all built-in WordPress templates. Custom page templates already known,
            // but can't be assigned in the same way. The same about 'single-x' templates.
            // But, both cases aren't issues, as they'll go with the 'page' template, and we'll process it correctly
            add_filter(sprintf('%s_template', $templateName), [$this, 'getPathToGeneralTemplate',]);
        }
    }

    public function addPageTemplates(array $templates, WP_Theme $theme, ?WP_Post $post, string $postType): array
    {
        return array_merge($templates, $this->custom);
    }

    public function printPage(): void
    {
        $currentTemplate = $this->getCurrentTemplate();

        if (!$currentTemplate) {
            $this->getLogger()->error('Can\'t print the template', [
                '$currentTemplate' => $currentTemplate,
                'supported' => $this->supported,
            ]);

            return;
        }

        if (is_callable($this->supported[$currentTemplate])) {
            call_user_func_array($this->supported[$currentTemplate], [$currentTemplate,]);
            return;
        }

        try {
            $blockInstance = $this->getFrontBlocksCreator()->create($this->supported[$currentTemplate]);

            if (!is_a($blockInstance, TemplateBlockInterface::class)) {
                throw new Exception("Instance doesn't implement TemplateBlockInterface");
            }
        } catch (Exception $exception) {
            $this->getLogger()->error('Value in the templates array is wrong', [
                'currentTemplate' => $currentTemplate,
                'value' => $this->supported[$currentTemplate],
                'errorMessage' => $exception->getMessage(),
            ]);

            return;
        }

        $blockInstance->loadByTemplate($currentTemplate);

        ob_start();

        $this->printHeader();

        $this->enqueueDefaultsBlock();
        $this->getRenderer()->render($blockInstance, [], true);

        $this->printFooter();

        $content = ob_get_clean();

        // 'late' filters to add data, so atm all blocks were rendered
        // used e.g. in Thumbnails
        $dynamicHead = apply_filters('thunder-wp__head', '');
        $dynamicBodyOpen = apply_filters('thunder-wp__body-open', '');

        $pageHtml = str_replace('<!--thunder-wp__head-->', $dynamicHead, $content);
        $pageHtml = str_replace('<!--thunder-wp__body-open-->', $dynamicBodyOpen, $pageHtml);

        echo $pageHtml;
    }

    public function maybePrintPage(string $template): string
    {
        if ($this->getPathToGeneralTemplate() !== $template) {
            return $template;
        }

        $this->printPage();

        return $template;
    }
}
