<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use LightSource\DataTypes\Interfaces\ValidationInterface;
use LightSource\DataTypes\Interfaces\ValidationResultInterface;
use LightSource\ThunderWP\Interfaces\DataTypesInterface;

class DataTypes extends \LightSource\DataTypes\DataTypes implements DataTypesInterface
{
    private bool $isStripSlashes;

    public function __construct(?ValidationInterface $validation = null)
    {
        parent::__construct($validation);

        $this->isStripSlashes = false;
    }


    protected function isStripSlashes(): bool
    {
        return $this->isStripSlashes;
    }

    public function clean(array $data, array $rules, array $excludedStringFields = []): array
    {
        if ($this->isStripSlashes) {
            // remove slashes added by WP
            $data = array_map('stripslashes_deep', $data);
        }

        return parent::clean($data, $rules, $excludedStringFields);
    }

    public function validate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = []
    ): ValidationResultInterface {
        if ($this->isStripSlashes) {
            // remove slashes added by WP
            $data = array_map('stripslashes_deep', $data);
        }

        return parent::validate(
            $data,
            $rules,
            $customFieldNames,
            $customErrorMessages
        );
    }

    public function cleanAndValidate(
        array $data,
        array $rules,
        array $customFieldNames = [],
        array $customErrorMessages = [],
        array $excludedStringFieldsFromClean = []
    ): ValidationResultInterface {
        if ($this->isStripSlashes) {
            // remove slashes added by WP
            $data = array_map('stripslashes_deep', $data);
        }

        return parent::cleanAndValidate(
            $data,
            $rules,
            $customFieldNames,
            $customErrorMessages,
            $excludedStringFieldsFromClean
        );
    }

    public function getConfigName(): string
    {
        return 'dataTypes';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->isStripSlashes = $configArguments['isStripSlashes'] ?? false;
    }
}
