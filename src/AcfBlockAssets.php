<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use DI\Container;
use LightSource\AcfBlocks\Interfaces\SettingsInterface as AcfBlocksSettingsInterface;
use LightSource\ThunderWP\Interfaces\{AcfBlockAssetsInterface, AcfBlockAssetsPageInterface, AcfBlockAssetsSettingsInterface};

class AcfBlockAssets implements AcfBlockAssetsInterface
{
    private ?AcfBlockAssetsSettingsInterface $settings;
    private ?AcfBlockAssetsPageInterface $page;
    private AcfBlocksSettingsInterface $acfBlocksSettings;
    private Container $container;

    public function __construct(AcfBlocksSettingsInterface $acfBlocksSettings, Container $container)
    {
        $this->acfBlocksSettings = $acfBlocksSettings;
        $this->settings = null;
        $this->page = null;
        $this->container = $container;
    }

    protected function getAcfBlocksSettings(): AcfBlocksSettingsInterface
    {
        return $this->acfBlocksSettings;
    }

    protected function getSettings(): ?AcfBlockAssetsSettingsInterface
    {
        return $this->settings;
    }

    protected function getPage(): ?AcfBlockAssetsPageInterface
    {
        return $this->page;
    }

    public function addPageCss(string $usedCss, int $queriedId): string
    {
        if (!is_page($queriedId)) {
            return $usedCss;
        }

        $additionalCss = $this->page->getCssCode();

        if (!$additionalCss) {
            return $usedCss;
        }

        // simple minification
        $additionalCss = str_replace(["\n", "\r"], '', $additionalCss);

        return $usedCss . "\n/* local */\n" . $additionalCss;
    }

    public function setAssetsVersion(): void
    {
        $assetsVersion = $this->settings->isStaging() ?
            (string)time() :
            $this->settings->getAssetsVersion();

        $this->acfBlocksSettings->setAssetsVersion($assetsVersion);
    }

    public function fixFrontFileNameForWooCommerceCheckout(): void
    {
        if (!function_exists('is_checkout') ||
            !function_exists('is_wc_endpoint_url') ||
            !is_checkout()) {
            return;
        }

        $isThankYou = is_wc_endpoint_url('order-received');
        $customResourceName = 'checkout';
        $customResourceName .= $isThankYou ?
            '-thank-you' :
            '';

        $this->acfBlocksSettings->setCustomFrontFileName($customResourceName);
    }

    public function setHooks(): void
    {
        // get_field will be available only since this hook
        add_action('acf/init', [$this, 'setAssetsVersion'], 20);

        // fix the styles conflict, due to a couple of different looks at one page
        add_action('template_redirect', [$this, 'fixFrontFileNameForWooCommerceCheckout']);

        // additional local css (e.g. spacing between elements)
        add_filter('LightSource_AcfBlocks_UsedCss', [$this, 'addPageCss'], 10, 2);
    }

    public function getConfigName(): string
    {
        return 'acfBlockAssets';
    }

    public function setConfigArguments(array $configArguments): void
    {
        if (isset($configArguments['settings'])) {
            $this->settings = $this->container->get($configArguments['settings']);
        }

        if (isset($configArguments['page'])) {
            $this->page = $this->container->get($configArguments['page']);
        }
    }
}
