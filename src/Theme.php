<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use LightSource\ThunderWP\Interfaces\ThemeInterface;
use Psr\Log\LoggerInterface;
use WP_Error;

class Theme implements ThemeInterface
{
    private LoggerInterface $logger;
    private array $optionPages;
    private array $supports;
    private array $fontsToPreload;
    private bool $isWithoutComments;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->optionPages = [];
        $this->supports = [];
        $this->fontsToPreload = [];
        $this->isWithoutComments = false;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->getLogger();
    }

    protected function getOptionPages(): array
    {
        return $this->optionPages;
    }

    protected function getSupports(): array
    {
        return $this->supports;
    }

    protected function getFrontsToPreload(): array
    {
        return $this->fontsToPreload;
    }

    public function isWithoutComments(): bool
    {
        return $this->isWithoutComments;
    }

    public function getConfigName(): string
    {
        return 'theme';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->optionPages = $configArguments['optionPages'] ?? [];
        $this->supports = $configArguments['supports'] ?? [];
        $this->fontsToPreload = $configArguments['fontsToPreload'] ?? [];
        $this->isWithoutComments = $configArguments['isWithoutComments'] ?? false;
    }

    public function setHooks(): void
    {
        add_action('wp_mail_failed', [$this, 'logMailError']);
        add_action('after_setup_theme', [$this, 'setupSupports',]);

        add_action('wp_head', [$this, 'preloadFonts',]);
        add_action('acf/init', [$this, 'addOptionPages']);

        if ($this->isWithoutComments) {
            add_filter('comments_open', '__return_false', 20);
            add_filter('pings_open', '__return_false', 20);
            add_action('admin_init', [$this, 'removeCommentsSupportFromAllPostTypes',]);
        }
    }

    public function removeCommentsSupportFromAllPostTypes(): void
    {
        $types = get_post_types();
        foreach ($types as $type) {
            if (!post_type_supports($type, 'comments')) {
                continue;
            }

            remove_post_type_support($type, 'comments');
            remove_post_type_support($type, 'trackbacks');
        }
    }

    public function printHtmlHeader(): void
    {
        echo '<!DOCTYPE html>';
        printf('<html %s>', get_language_attributes());
        echo '<head>';

        printf('<meta charset="%s"/>', get_bloginfo('charset', 'display'));
        echo '<meta name="viewport" content="width=device-width, initial-scale=1" />';

        printf('<title>%s</title>', wp_title('&raquo;', false));

        printf(
            '<link rel="alternate" type="application/rss+xml" title="%s RSS Feed" href="%s"/>',
            get_bloginfo('name', 'display'),
            get_bloginfo('rss2_url', 'display')
        );

        call_user_func_array('wp_head', []);
        echo '<!--acf_blocks_css-->';
        echo '<!--thunder-wp__head-->';

        echo '</head>';
        printf('<body class="%s">', join(" ", get_body_class()));
        call_user_func_array('wp_body_open', []);
        echo '<!--thunder-wp__body-open-->';
    }

    public function printHtmlFooter(): void
    {
        call_user_func_array('wp_footer', []);

        echo '</body></html>';
    }

    public function addOptionPages(): void
    {
        if (!function_exists('acf_add_options_page')) {
            return;
        }

        foreach ($this->optionPages as $optionPage) {
            acf_add_options_page($optionPage);
        }
    }

    public function preloadFonts(): void
    {
        foreach ($this->fontsToPreload as $font) {
            printf("<link rel='preload' href='%s'>", $font);
        }
    }

    public function logMailError(WP_Error $error): void
    {
        $this->logger->error('Fail to send an email', [
            'error' => $error,
        ]);
    }

    public function setupSupports(): void
    {
        foreach ($this->supports as $feature) {
            add_theme_support($feature);
        }
    }
}
