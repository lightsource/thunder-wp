<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use Exception;
use LightSource\FrontBlocks\Interfaces\CreatorInterface as FrontBlocksCreatorInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;
use Psr\Log\LoggerInterface;
use LightSource\ThunderWP\Interfaces\{WooBlockInterface, WooTemplatesInterface};

class WooTemplates extends CommonTemplates implements WooTemplatesInterface
{
    private array $supported;

    public function __construct(
        FrontBlocksCreatorInterface $frontBlocksCreator,
        LoggerInterface $logger,
        RendererInterface $renderer
    ) {
        parent::__construct($frontBlocksCreator, $logger, $renderer);

        $this->supported = [];
    }

    protected function printTemplateBlock(string $templateName, array $args): void
    {
        if (is_callable($this->supported[$templateName])) {
            call_user_func_array($this->supported[$templateName], [$templateName, $args,]);
            return;
        }

        try {
            $blockInstance = $this->getFrontBlocksCreator()->create($this->supported[$templateName]);

            if (!is_a($blockInstance, WooBlockInterface::class)) {
                throw new Exception("Instance doesn't implement WooBlockInterface");
            }
        } catch (Exception $exception) {
            $this->getLogger()->error('Value in the wooTemplates array is wrong', [
                'templateName' => $templateName,
                'value' => $this->supported[$templateName],
                'errorMessage' => $exception->getMessage(),
            ]);

            return;
        }

        $blockInstance->loadByWooCommerce($templateName, $args);
        $this->getRenderer()->render($blockInstance, [], true);
    }

    public function getConfigName(): string
    {
        return 'wooTemplates';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->supported = $configArguments['supported'] ?? [];
    }

    public function setHooks(): void
    {
        add_filter('wc_get_template', [$this, 'maybeOverrideTemplate',], 10, 5);

        add_action('woocommerce_before_template_part', [$this, 'maybePrintTemplateBlock'], 10, 4);
    }

    public function maybePrintTemplateBlock($templateName, $templatePath, $located, $args): void
    {
        if ($this->getPathToGeneralTemplate() !== $located) {
            return;
        }

        $this->printTemplateBlock($templateName, $args);
    }

    public function maybeOverrideTemplate($template, $templateName, $args, $templatePath, $defaultPath): string
    {
        if (!key_exists($templateName, $this->supported)) {
            return $template;
        }

        return $this->getPathToGeneralTemplate();
    }
}
