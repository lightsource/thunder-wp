<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use LightSource\FrontBlocks\Interfaces\CreatorInterface as FrontBlocksCreatorInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;
use Psr\Log\LoggerInterface;

class CommonTemplates
{
    private FrontBlocksCreatorInterface $frontBlocksCreator;
    private LoggerInterface $logger;
    private RendererInterface $renderer;

    public function __construct(
        FrontBlocksCreatorInterface $frontBlocksCreator,
        LoggerInterface             $logger,
        RendererInterface           $renderer
    )
    {
        $this->frontBlocksCreator = $frontBlocksCreator;
        $this->logger = $logger;
        $this->renderer = $renderer;
    }

    protected function getFrontBlocksCreator(): FrontBlocksCreatorInterface
    {
        return $this->frontBlocksCreator;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getRenderer(): RendererInterface
    {
        return $this->renderer;
    }

    public function getPathToGeneralTemplate(): string
    {
        return __DIR__ . '/general-template.php';
    }
}
