<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use DI\Container;
use Exception;
use LightSource\AcfBlocks\Interfaces\SettingsInterface as AcfBlocksSettingsInterface;
use LightSource\AcfBlocks\Settings as AcfBlocksSettings;
use LightSource\AcfGroups\Creator;
use LightSource\AcfGroups\Interfaces\AcfGroupInterface;
use LightSource\AcfGroups\Interfaces\CreatorInterface as AcfGroupsCreatorInterface;
use LightSource\DataTypes\Interfaces\DataTypesInterface;
use LightSource\FrontBlocks\{Interfaces\CreatorInterface as FrontBlocksCreatorInterface,
    Interfaces\FrontBlocksInterface,
    Interfaces\RendererInterface,
    Interfaces\SettingsInterface as FrontBlocksSettingsInterface,
    Settings as FrontBlocksSettings
};
use LightSource\Log\{Interfaces\SettingsInterface as LogSettingsInterface, Log, Settings as LogSettings};
use LightSource\ThunderWP\Interfaces\{Factories\AcfBlocksFactoryInterface,
    Factories\AcfGroupsFactoryInterface,
    Factories\FactoryInterface,
    Factories\FrontBlocksFactoryInterface,
    Factories\LoggerFactoryInterface,
    HooksInterface,
    AcfBlockAssetsInterface,
    ModuleInterface,
    AcfBlockAssetsPageInterface,
    AcfBlockAssetsSettingsInterface,
    TemplatesInterface,
    ThunderWPInterface,
    ImagesInterface,
    ThemeInterface,
    TemplatesPageInterface,
    WooTemplatesInterface
};
use LightSource\ThunderWP\Factories\{AcfBlocksFactory,
    AcfGroupsFactory,
    FrontBlocksFactory,
    LoggerFactory,
};

class ThunderWP implements ThunderWPInterface
{
    /**
     * @param array $config
     * @return void
     * @throws Exception
     */
    public function load(array $config): void
    {
        $container = $config['thunder']['container'] ?? null;
        $instances = $config['thunder']['instances'] ?? [];

        if (!is_a($container, Container::class)) {
            throw new Exception('Container argument is wrong.');
        }

        foreach ($instances as $keyOrInterface => $instanceClass) {
            $instance = $container->get($instanceClass);

            if (!is_numeric($keyOrInterface)) {
                $container->set($keyOrInterface, $instance);
            }

            if (is_a($instance, ModuleInterface::class)) {
                $instance->setConfigArguments($config[$instance->getConfigName()] ?? []);
            }

            if (is_a($instance, HooksInterface::class)) {
                $instance->setHooks();
            }

            if (is_a($instance, FactoryInterface::class)) {
                $targetInstance = $instance->makeInstance();

                $containerProperties = $instance->getContainerProperties($targetInstance);

                foreach ($containerProperties as $containerInterface => $containerInstance) {
                    $container->set($containerInterface, $containerInstance);
                }
            }
        }
    }

    /**
     * @param string $pathToConfig
     * @return void
     * @throws Exception
     */
    public function loadFromFile(string $pathToConfig): void
    {
        if (!file_exists($pathToConfig)) {
            throw new Exception('Config file does not exists');
        }

        $config = include $pathToConfig;

        if (!is_array($config)) {
            throw new Exception('Config file must return an array');
        }

        $this->load($config);
    }
}
