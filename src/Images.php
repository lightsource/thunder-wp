<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use Exception;
use LightSource\ThunderWP\Interfaces\ImagesInterface;
use Psr\Log\LoggerInterface;

class Images implements ImagesInterface
{
    private array $renderedSvg;
    private string $svgHtml;
    private LoggerInterface $logger;
    private array $excludedSizes;
    private string $folder;
    private int $imageIdToSkipWebp;

    public function __construct(LoggerInterface $logger)
    {
        $this->renderedSvg = [];
        $this->svgHtml = '';
        $this->logger = $logger;
        $this->excludedSizes = [];
        $this->folder = '';
        $this->imageIdToSkipWebp = 0;
    }

    protected function getRenderedSvg(): array
    {
        return $this->renderedSvg;
    }

    protected function getSvgHtml(): string
    {
        return $this->svgHtml;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getImageIdToSkipWebp(): int
    {
        return $this->imageIdToSkipWebp;
    }

    public function getConfigName(): string
    {
        return 'images';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->folder = $configArguments['folder'] ?? '';
        $this->excludedSizes = $configArguments['excludedSizes'] ?? [];
    }

    public function setHooks(): void
    {
        // fixme
        add_filter('thunder-wp__body-open', [$this, 'addSvgHtml',]);
        add_filter('thunder-wp__head', [$this, 'addSvgCss',]);

        add_filter('intermediate_image_sizes', [$this, 'removeExtraImageSizes',]);

        add_filter(
            'wp_get_attachment_image_src',
            [$this, 'convertImageSrcToWebp'],
            10,
            4
        );
        add_filter(
            'wp_calculate_image_srcset',
            [$this, 'convertImageSrcsetToWebp'],
            10,
            5
        );
    }

    public function getInlineSvg(string $file, string $classes = ''): string
    {
        $inlineSvg = (string)file_get_contents($file);

        if (!$inlineSvg) {
            $this->logger->warning('Svg file is missing', [
                'file' => $file,
                'classes' => $classes,
            ]);
            return '';
        }

        // get the name
        $name = explode('/', $file);
        $name = $name[count($name) - 1];

        $nameParts = explode('.', $name);
        $extension = $nameParts[count($nameParts) - 1];

        // it's important to check the extension, if we print e.g. '.png' it'll cause view issues in browser
        if ('svg' !== $extension) {
            $this->logger->warning('Svg file is wrong', [
                'file' => $file,
                'classes' => $classes,
            ]);

            return '';
        }

        // remove the extension
        $id = 'svg-' . $nameParts[0];

        if (!key_exists($id, $this->renderedSvg)) {
            preg_match('/viewBox=["\']{1}([0-9 ]*)["\']/', $inlineSvg, $viewBoxData);

            $this->renderedSvg[$id] = $viewBoxData[1] ?? '';

            // don't return this for the first case, to make sure all used svg will be in one place (after body open)
            // it's important, if e.g. one element was requested, but not showed, then others' svg (with the same picture) would be broken
            $this->svgHtml .= str_replace(
                '<svg ',
                sprintf('<svg id="%s" class="thunder-wp__svg" ', $id),
                $inlineSvg
            );
        }

        return sprintf(
            '<svg viewBox="%s" class="%s"><use href="#%s" xmlns="http://www.w3.org/2000/svg"></use></svg>',
            $this->renderedSvg[$id],
            $classes,
            $id
        );
    }

    public function getThemeInlineSvg(string $name, string $classes = ''): string
    {
        return $this->getInlineSvg($this->folder . '/' . $name, $classes);
    }

    public function getAttachmentInlineSvg(int $imageId, string $classes = ''): string
    {
        return $this->getInlineSvg((string)get_attached_file($imageId), $classes);
    }

    public function getImage(
        int $imageId,
        string $size,
        string $classes = '',
        bool $isWithoutLazy = false,
        bool $isSkipWebp = false
    ): string {
        $class = 'image';
        $class .= $classes ?
            ' ' . $classes :
            '';

        if ($isSkipWebp) {
            $this->imageIdToSkipWebp = $imageId;
        }

        $image = wp_get_attachment_image($imageId, $size, false, [
            'class' => $class,
            // use the default values instead of empty (not 'false'), to avoid late dynamic replace by WP
            'loading' => $isWithoutLazy ?
                'eager' :
                'lazy',
            'decoding' => $isWithoutLazy ?
                'sync' :
                'async',
        ]);

        if ($isSkipWebp) {
            $this->imageIdToSkipWebp = 0;
        }

        return $image;
    }

    // use 'getImage()' wherever it's possible, this method e.g. for getting icons for emails
    public function getImageUrl(int $imageId, string $size, bool $isSkipWebp = false): string
    {
        if ($isSkipWebp) {
            $this->imageIdToSkipWebp = $imageId;
        }

        $url = (string)wp_get_attachment_image_url($imageId, $size);

        if ($isSkipWebp) {
            $this->imageIdToSkipWebp = 0;
        }

        return $url;
    }

    public function addSvgHtml(string $html): string
    {
        return $html . $this->svgHtml;
    }

    public function addSvgCss(string $html): string
    {
        if (!$this->svgHtml) {
            return $html;
        }

        return $html . "<style id='thunder-wp'>.thunder-wp__svg{width:0;height:0;position:absolute;z-index:-1;left:-100vw;}</style>";
    }

    public function removeExtraImageSizes(array $sizes): array
    {
        return array_diff($sizes, $this->excludedSizes);
    }

    /**
     * @param array|false $image
     * @param int $attachmentId
     * @param $size
     * @param bool $icon
     *
     * @return array|false
     */
    public function convertImageSrcToWebp($image, int $attachmentId, $size, bool $icon)
    {
        if (!class_exists('\WebPExpress\AlterHtmlHelper') ||
            $this->imageIdToSkipWebp === $attachmentId) {
            return $image;
        }

        if (!$image ||
            (false === strpos($image[0], '.png') &&
                false === strpos($image[0], '.jpg') &&
                false === strpos($image[0], '.jpeg'))) {
            return $image;
        }
        $image[0] = \WebPExpress\AlterHtmlHelper::getWebPUrl($image[0], $image[0]);

        return $image;
    }

    public function convertImageSrcsetToWebp(
        array $sources,
        array $sizeArray,
        string $imageSrc,
        array $imageMeta,
        int $attachmentId
    ): array {
        if (!class_exists('\WebPExpress\AlterHtmlHelper') ||
            $this->imageIdToSkipWebp === $attachmentId) {
            return $sources;
        }

        foreach ($sources as $width => $source) {
            $url = $source['url'] ?? '';

            if (!$url ||
                (false === strpos($url, '.png') &&
                    false === strpos($url, '.jpg') &&
                    false === strpos($url, '.jpeg'))) {
                continue;
            }

            $sources[$width]['url'] = \WebPExpress\AlterHtmlHelper::getWebPUrl($url, $url);
        }

        return $sources;
    }
}
