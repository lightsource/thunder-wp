<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\ThunderWP\Interfaces\WpObjects\CptInterface;
use WP_Query;

class Cpt extends CptData implements CptInterface
{
    private array $columns;
    private array $sortableColumns;

    public function __construct()
    {
        parent::__construct();

        $this->columns = [];
        $this->sortableColumns = [];
    }

    protected function getColumns(): array
    {
        return $this->columns;
    }

    protected function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    protected function getSortableColumns(): array
    {
        return $this->sortableColumns;
    }

    protected function setSortableColumns(array $sortableColumns): void
    {
        $this->sortableColumns = $sortableColumns;
    }

    protected function modifyColumnOrderQuery(string $columnSlug, WP_Query $query): void
    {
    }

    protected function modifySearchQuery(string $searchValue, WP_Query $query): void
    {
    }

    protected function getLabels(): array
    {
        $labels = [
            'name' => $this->getName(),
            'singular_name' => $this->getSingularName(),
            'menu_name' => $this->getName(),
            'all_items' => sprintf('All %s', $this->getName()),
            'add_new' => 'Add new',
            'add_new_item' => sprintf('Add new %s', $this->getSingularName()),
            'edit_item' => sprintf('Edit %s', $this->getSingularName()),
            'new_item' => sprintf('New %s', $this->getSingularName()),
            'view_item' => sprintf('View %s', $this->getSingularName()),
            'view_items' => sprintf('View %s', $this->getName()),
            'search_items' => sprintf('Search %s', $this->getName()),
            'not_found' => sprintf('No %s found', $this->getName()),
            'not_found_in_trash' => sprintf('No %s found in trash', $this->getName()),
            'parent' => sprintf('Parent %s', $this->getSingularName()),
            'featured_image' => sprintf('Featured image for this %s', $this->getSingularName()),
            'set_featured_image' => sprintf('Set featured image for this %s', $this->getSingularName()),
            'remove_featured_image' => sprintf('Remove featured image for this %s', $this->getSingularName()),
            'use_featured_image' => sprintf('Use as featured image for this %s', $this->getSingularName()),
            'archives' => sprintf('%s archives', $this->getSingularName()),
            'insert_into_item' => sprintf('Insert into %s', $this->getSingularName()),
            'uploaded_to_this_item' => sprintf('Upload to this %s', $this->getSingularName()),
            'filter_items_list' => sprintf('Filter %s list', $this->getName()),
            'items_list_navigation' => sprintf('%s list navigation', $this->getName()),
            'items_list' => sprintf('%s list', $this->getName()),
            'attributes' => sprintf('%s attributes', $this->getName()),
            'name_admin_bar' => $this->getSingularName(),
            'item_published' => sprintf('%s published', $this->getSingularName()),
            'item_published_privately' => sprintf('%s published privately.', $this->getSingularName()),
            'item_reverted_to_draft' => sprintf('%s reverted to draft.', $this->getSingularName()),
            'item_scheduled' => sprintf('%s scheduled', $this->getSingularName()),
            'item_updated' => sprintf('%s updated.', $this->getSingularName()),
            'parent_item_colon' => sprintf('Parent %s:', $this->getSingularName()),
        ];

        return array_merge($labels, $this->getCustomLabels());
    }

    protected function getArguments(): array
    {
        $arguments = [
            'label' => $this->getName(),
            'labels' => $this->getLabels(),
            'description' => '',
            'public' => !$this->isForAdminsOnly(),
            'publicly_queryable' => !$this->isForAdminsOnly(),
            'show_ui' => true,
            'show_in_rest' => false,
            'rest_base' => '',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'rest_namespace' => 'wp/v2',
            'has_archive' => false,
            'show_in_menu' => true,
            'show_in_nav_menus' => !$this->isForAdminsOnly(),
            'delete_with_user' => false,
            'exclude_from_search' => $this->isForAdminsOnly(),
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'can_export' => false,
            'rewrite' => false,
            'query_var' => false,
            'menu_icon' => '',
            'supports' => ['title',],
            'taxonomies' => [],
            'show_in_graphql' => false,
        ];

        return array_merge($arguments, $this->getCustomSettings());
    }

    public function setHooks(): void
    {
        parent::setHooks();

        add_action(
            sprintf('manage_%s_posts_custom_column', $this->getSlug()),
            [$this, 'printColumn',],
            10,
            2
        );
        add_action('pre_get_posts', [$this, 'modifyQuery']);

        add_filter(
            sprintf('views_edit-%s', $this->getSlug()),
            [$this, 'printPostTypeDescription',]
        );
        add_filter(
            sprintf('manage_%s_posts_columns', $this->getSlug()),
            [$this, 'getAvailableColumns',]
        );
        add_filter(
            sprintf('manage_edit-%s_sortable_columns', $this->getSlug()),
            [$this, 'getAvailableSortableColumns',]
        );
    }

    public function signup(): void
    {
        register_post_type($this->getSlug(), $this->getArguments());
    }

    public function printColumn(string $columnSlug, int $postId): void
    {
    }

    public function printPostTypeDescription($views)
    {
        $description = $this->getCustomSettings()['description'] ?? '';
        if ($description) {
            printf('<p>%s</p>', esc_html($description));
        }

        // return original input unchanged
        return $views;
    }

    public function getAvailableColumns(array $columns): array
    {
        $availableColumns = array_merge($columns, $this->columns);

        // unset empty
        foreach ($availableColumns as $columnSlug => $columnName) {
            if ($columnName) {
                continue;
            }

            unset($availableColumns[$columnSlug]);
        }

        return $availableColumns;
    }

    public function getAvailableSortableColumns(array $sortableColumns): array
    {
        $availableSortableColumns = array_merge($sortableColumns, $this->sortableColumns);

        // unset empty
        foreach ($availableSortableColumns as $columnSlug => $columnName) {
            if ($columnName) {
                continue;
            }

            unset($availableSortableColumns[$columnSlug]);
        }

        return $availableSortableColumns;
    }

    public function modifyQuery(WP_Query $query): void
    {
        $postType = $query->query_vars['post_type'] ?? '';

        if (!is_admin() ||
            $this->getSlug() !== $postType ||
            !$query->is_main_query()) {
            return;
        }

        $orderColumnSlug = $query->get('orderby');

        if ($orderColumnSlug) {
            $this->modifyColumnOrderQuery($orderColumnSlug, $query);
        }

        if ($query->is_search) {
            $searchValue = $query->query_vars['s'];
            $this->modifySearchQuery($searchValue, $query);
        }
    }
}
