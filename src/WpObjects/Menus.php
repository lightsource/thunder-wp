<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\FrontBlocks\Interfaces\TemplateInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuDataInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemBlockInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenusInterface;
use WP_Post;

class Menus implements MenusInterface
{
    private MenuDataInterface $menuData;
    private MenuItemInterface $menuItem;
    private TemplateInterface $template;

    public function __construct(MenuDataInterface $menuData, MenuItemInterface $menuItem, TemplateInterface $template)
    {
        $this->menuData = $menuData;
        $this->menuItem = $menuItem;
        $this->template = $template;
    }

    protected function getMenuItem(): MenuItemInterface
    {
        return $this->menuItem;
    }

    protected function getMenuData(): MenuDataInterface
    {
        return $this->menuData;
    }

    protected function getTemplate(): TemplateInterface
    {
        return $this->template;
    }

    /**
     * @param WP_Post[] $items
     * @param int $parentId
     * @return WP_Post[]
     */
    protected function spliceItemsWithParent(array &$items, int $parentId): array
    {
        $itemsWithParent = [];
        foreach ($items as $key => $item) {
            $itemParentId = (int)($item->menu_item_parent ?? 0);

            if ($itemParentId !== $parentId) {
                continue;
            }

            $itemsWithParent[] = $item;

            unset($items[$key]);
        }

        return $itemsWithParent;
    }

    public function getMenu(string $menuSlug): MenuData
    {
        $wpMenuItems = wp_get_nav_menu_items($menuSlug);

        $menuData = $this->menuData->getDeepClone();
        $menuData->setSlug($menuSlug);

        $topLevelItems = $this->spliceItemsWithParent($wpMenuItems, 0);

        foreach ($topLevelItems as $topLevelItem) {
            $menuItem = $this->menuItem->getDeepClone();

            $menuItem->setWpObject($topLevelItem);

            $childItems = $this->spliceItemsWithParent($wpMenuItems, $topLevelItem->ID);

            foreach ($childItems as $childItem) {
                $childMenuItem = $this->menuItem->getDeepClone();

                $childMenuItem->setWpObject($childItem);
                $childMenuItem->setParent($menuItem);

                $grandChildrenItems = $this->spliceItemsWithParent($wpMenuItems, $childItem->ID);

                foreach ($grandChildrenItems as $grandChildItem) {
                    $grandChildMenuItem = $this->menuItem->getDeepClone();

                    $grandChildMenuItem->setWpObject($grandChildItem);
                    $grandChildMenuItem->setParent($childMenuItem);

                    $childMenuItem->addChild($grandChildMenuItem);
                }

                $menuItem->addChild($childMenuItem);
            }

            $menuData->addItem($menuItem);
        }

        return $menuData;
    }

    public function getMenuTwigArguments(
        string $menuSlug,
        MenuItemBlockInterface $firstLevel,
        MenuItemBlockInterface $secondLevel,
        MenuItemBlockInterface $thirdLevel
    ): array {
        $twigArguments = [];

        $menuData = $this->getMenu($menuSlug);

        $items = $menuData->getItems();
        foreach ($items as $menuItem) {
            $children = $menuItem->getChildren();
            $twigChildren = [];

            $itemBlock = $firstLevel->getDeepClone();
            $itemBlock->loadByMenuItem($menuItem);

            foreach ($children as $child) {
                $childItemBlock = $secondLevel->getDeepClone();
                $childItemBlock->loadByMenuItem($child);

                $grandChildren = $child->getChildren();
                $twigGrandChildren = [];

                foreach ($grandChildren as $grandChild) {
                    $grandChildItemBlock = $thirdLevel->getDeepClone();
                    $grandChildItemBlock->loadByMenuItem($grandChild);

                    $twigGrandChildren[] = [
                        'item' => $this->template->getArgs($grandChildItemBlock),
                        'isActive' => $grandChild->isActive(),
                        'isChildActive' => false,
                        'children' => [],
                    ];
                }

                $twigChildren[] = [
                    'item' => $this->template->getArgs($childItemBlock),
                    'isActive' => $child->isActive(),
                    'isChildActive' => $child->isChildActive(),
                    'children' => $twigGrandChildren,
                ];
            }

            $twigArguments[] = [
                'item' => $this->template->getArgs($itemBlock),
                'isActive' => $menuItem->isActive(),
                'isChildActive' => $menuItem->isChildActive(),
                'children' => $twigChildren,
            ];
        }

        return $twigArguments;
    }
}
