<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\ThunderWP\Interfaces\WpObjects\MenuDataInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemInterface;
use WP_Post;

class MenuItem implements MenuItemInterface
{
    private ?WP_Post $wpObject;
    private ?MenuItemInterface $parent;
    /**
     * @var MenuItem[]
     */
    private array $children;
    private bool $isActive;
    private bool $isChildActive;

    public function __construct()
    {
        $this->wpObject = null;
        $this->parent = null;
        $this->children = [];
        $this->isActive = false;
        $this->isChildActive = false;
    }

    protected function isActiveItem(WP_Post $menuItem): bool
    {
        $postsPageId = (int)get_option('page_for_posts');
        $objectId = (int)($menuItem->object_id ?? 0);

        // active if the current menu is for current page, or
        // the current menu for blog and the current page is post or
        // the current menu for blog and the current page is author page
        // the current menu for blog and the current page is category page

        if (($objectId && get_queried_object_id() === $objectId) ||
            ($objectId === $postsPageId && is_singular('post')) ||
            ($objectId === $postsPageId && is_author()) ||
            ($objectId === $postsPageId && is_category())) {
            return true;
        }

        return false;
    }

    public function getWpObject(): ?WP_Post
    {
        return $this->wpObject;
    }

    public function getParent(): ?MenuItemInterface
    {
        return $this->parent;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isChildActive(): bool
    {
        return $this->isChildActive;
    }

    public function getUrl(): string
    {
        return $this->wpObject->url ?? '';
    }

    public function getTitle(): string
    {
        return $this->wpObject->title ?? '';
    }

    public function isBlank(): bool
    {
        return '_blank' === ($this->wpObject->target ?? '');
    }

    public function getDeepClone(): MenuItemInterface
    {
        $clone = clone $this;

        if ($clone->wpObject) {
            $clone->wpObject = clone $clone->wpObject;
        }

        if ($clone->parent) {
            $clone->parent = clone $clone->parent;
        }

        foreach ($clone->children as &$child) {
            $child = $child->getDeepClone();
        }

        return $clone;
    }

    public function setWpObject(?WP_Post $wpObject): void
    {
        $this->wpObject = $wpObject;

        $this->isActive = $this->isActiveItem($wpObject);
    }

    public function setParent(?MenuItemInterface $parent): void
    {
        $this->parent = $parent;
    }

    public function addChild(MenuItemInterface $item): void
    {
        $this->children[] = $item;

        $this->isChildActive = ($item->isActive() ||
            $item->isChildActive()) ?
            true :
            $this->isChildActive;
    }
}
