<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\ThunderWP\Interfaces\WpObjects\TaxonomyInterface;

class Taxonomy extends CptData implements TaxonomyInterface
{
    private array $targetPostTypes;

    public function __construct()
    {
        parent::__construct();

        $this->targetPostTypes = [];
    }

    protected function getTargetPostTypes(): array
    {
        return $this->targetPostTypes;
    }

    protected function setTargetPostTypes(array $targetPostTypes): void
    {
        $this->targetPostTypes = $targetPostTypes;
    }

    protected function getLabels(): array
    {
        $labels = [
            'name' => $this->getName(),
            'singular_name' => $this->getSingularName(),
            'menu_name' => $this->getName(),
            'all_items' => sprintf('All %s', $this->getName()),
            'edit_item' => sprintf('Edit %s', $this->getSingularName()),
            'view_item' => sprintf('View %s', $this->getSingularName()),
            'update_item' => sprintf('Update %s name', $this->getSingularName()),
            'add_new_item' => sprintf('Add new %s', $this->getSingularName()),
            'new_item_name' => sprintf('New %s name', $this->getSingularName()),
            'parent_item' => sprintf('Parent %s', $this->getSingularName()),
            'parent_item_colon' => sprintf('Parent %s:', $this->getSingularName()),
            'search_items' => sprintf('Search %s', $this->getName()),
            'popular_items' => sprintf('Popular %s', $this->getName()),
            'separate_items_with_commas' => sprintf('Separate %s with commas', $this->getName()),
            'add_or_remove_items' => sprintf('Add or remove %s', $this->getName()),
            'choose_from_most_used' => sprintf('Choose from the most used  %s', $this->getName()),
            'not_found' => sprintf('No %s found', $this->getName()),
            'no_terms' => sprintf('No %s', $this->getName()),
            'items_list_navigation' => sprintf('%s list navigation', $this->getName()),
            'items_list' => sprintf('%s list', $this->getName()),
            'back_to_items' => sprintf('Back to %s', $this->getName()),
            'name_field_description' => 'The name is how it appears on your site.',
            'parent_field_description' => 'Assign a parent term to create a hierarchy. The term Jazz, for example, would be the parent of Bebop and Big Band.',
            'slug_field_description' => 'The slug is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.',
            'desc_field_description' => 'The description is not prominent by default; however, some themes may show it.',
        ];

        return array_merge($labels, $this->getCustomLabels());
    }

    protected function getArguments(): array
    {
        $arguments = [
            'label' => $this->getName(),
            'labels' => $this->getLabels(),
            'public' => !$this->isForAdminsOnly(),
            'publicly_queryable' => !$this->isForAdminsOnly(),
            'hierarchical' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => !$this->isForAdminsOnly(),
            'query_var' => false,
            'rewrite' => false,
            'show_admin_column' => false,
            'show_in_rest' => false,
            'show_tagcloud' => false,
            'rest_base' => $this->getSlug(),
            'rest_controller_class' => 'WP_REST_Terms_Controller',
            'rest_namespace' => 'wp/v2',
            'show_in_quick_edit' => false,
            'sort' => false,
            'show_in_graphql' => false,
        ];

        return array_merge($arguments, $this->getCustomSettings());
    }

    public function signup(): void
    {
        register_taxonomy($this->getSlug(), $this->targetPostTypes, $this->getArguments());
    }
}
