<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\ThunderWP\Interfaces\WpObjects\CptDataInterface;

abstract class CptData implements CptDataInterface
{
    const SLUG = '';

    private string $name;
    private string $singularName;
    private bool $isForAdminsOnly;
    private array $customSettings;
    private array $customLabels;

    public function __construct()
    {
        $this->name = '';
        $this->singularName = '';
        $this->isForAdminsOnly = false;
        $this->customSettings = [];
        $this->customLabels = [];
    }

    protected function getName(): string
    {
        return $this->name;
    }

    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    protected function getSingularName(): string
    {
        return $this->singularName;
    }

    protected function setSingularName(string $singularName): void
    {
        $this->singularName = $singularName;
    }

    protected function isForAdminsOnly(): bool
    {
        return $this->isForAdminsOnly;
    }

    protected function setIsForAdminsOnly(bool $isForAdminsOnly): void
    {
        $this->isForAdminsOnly = $isForAdminsOnly;
    }

    protected function getCustomSettings(): array
    {
        return $this->customSettings;
    }

    protected function setCustomSettings(array $customSettings, bool $isMerge = true): void
    {
        if (!$isMerge) {
            $this->customSettings = $customSettings;
            return;
        }

        $this->customSettings = array_merge($this->customSettings, $customSettings);
    }

    protected function getCustomLabels(): array
    {
        return $this->customLabels;
    }

    protected function setCustomLabels(array $customLabels): void
    {
        $this->customLabels = $customLabels;
    }

    public function getSlug(): string
    {
        return static::SLUG;
    }

    public function setHooks(): void
    {
        add_action('init', [$this, 'signup']);
    }

    abstract public function signup();
}
