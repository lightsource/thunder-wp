<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\WpObjects;

use LightSource\ThunderWP\Interfaces\WpObjects\MenuDataInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemInterface;

class MenuData implements MenuDataInterface
{
    /**
     * @var MenuItemInterface[]
     */
    private array $items;
    private string $slug;

    public function __construct()
    {
        $this->items = [];
        $this->slug = '';
    }

    public function addItem(MenuItemInterface $item): void
    {
        $this->items[] = $item;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getDeepClone(): MenuDataInterface
    {
        $clone = clone $this;

        foreach ($this->items as &$item) {
            $item = $item->getDeepClone();
        }

        return $clone;
    }
}
