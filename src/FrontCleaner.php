<?php

declare(strict_types=1);

namespace LightSource\ThunderWP;

use LightSource\ThunderWP\Interfaces\FrontCleanerInterface;

class FrontCleaner implements FrontCleanerInterface
{
    private array $settings;

    public function __construct()
    {
        $this->settings = [];
    }

    protected function getSettings(): array
    {
        return $this->settings;
    }

    protected function isOptionPresent(string $option, string $source): bool
    {
        $data = $this->settings[$source] ?? [];

        return in_array($option, $data, true);
    }

    protected function isWooLoaded(): bool
    {
        return function_exists('is_woocommerce');
    }

    protected function isWooPage(): bool
    {
        if (!$this->isWooLoaded()) {
            return false;
        }

        return is_woocommerce() ||
            is_cart() ||
            is_checkout() ||
            is_account_page();
    }

    protected function cleanWp(): void
    {
        if ($this->isOptionPresent('blocksStyles', 'wp')) {
            add_action('wp_enqueue_scripts', function () {
                wp_dequeue_style('wp-block-library');
                wp_dequeue_style('wp-block-library-theme');
                wp_dequeue_style('storefront-gutenberg-blocks');
                wp_dequeue_style('classic-theme-styles');
            }, 100);
        }

        if ($this->isOptionPresent('emojiStyles', 'wp')) {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');
        }

        if ($this->isOptionPresent('duotone', 'wp')) {
            remove_action('wp_body_open', 'wp_global_styles_render_svg_filters');
        }

        if ($this->isOptionPresent('versionInHead', 'wp')) {
            remove_action('wp_head', 'wp_generator');
        }
    }

    protected function cleanYoast(): void
    {
        if ($this->isOptionPresent('versionInHead', 'yoast')) {
            add_filter('wpseo_debug_markers', '__return_false');
        }
    }

    protected function cleanWooCommerce(): void
    {
        if ($this->isOptionPresent('assetsOnUnrelatedPages', 'wooCommerce')) {
            add_action('wp_print_scripts', function () {
                if (!$this->isWooLoaded() ||
                    $this->isWooPage()) {
                    return;
                }

                wp_dequeue_script('wc-add-to-cart');
                wp_dequeue_script('jquery-blockui');
                wp_dequeue_script('jquery-placeholder');
                wp_dequeue_script('woocommerce');
                wp_dequeue_script('jquery-cookie');
                wp_dequeue_script('wc-cart-fragments');
            }, 100);
        }

        if ($this->isOptionPresent('assetsOnUnrelatedPages', 'wooCommerce') ||
            $this->isOptionPresent('styles', 'wooCommerce')) {
            add_action('wp_enqueue_scripts', function () {
                $isDisabledByUnrelatedRule = !$this->isWooPage() &&
                    $this->isOptionPresent('assetsOnUnrelatedPages', 'wooCommerce');

                if (!$this->isWooLoaded() ||
                    (!$isDisabledByUnrelatedRule && !$this->isOptionPresent('styles', 'wooCommerce'))) {
                    return;
                }

                wp_dequeue_style('woocommerce-layout');
                wp_dequeue_style('woocommerce-smallscreen');
                wp_dequeue_style('woocommerce-general');
                wp_dequeue_style('wc-blocks-style');
            }, 100);
        }
    }

    protected function cleanContactForm7(): void
    {
        if ($this->isOptionPresent('assetsOnUnrelatedPages', 'contactForm7') ||
            $this->isOptionPresent('styles', 'contactForm7')) {
            add_action('wp_enqueue_scripts', function () {
                global $post;

                $isRelatedPage = is_a($post, 'WP_Post') &&
                    has_shortcode($post->post_content, 'contact-form-7');

                $isDisabledByUnrelatedRule = !$isRelatedPage &&
                    $this->isOptionPresent('assetsOnUnrelatedPages', 'contactForm7');

                if ($isDisabledByUnrelatedRule ||
                    $this->isOptionPresent('styles', 'contactForm7')) {
                    wp_dequeue_style('contact-form-7');
                }

                if ($isDisabledByUnrelatedRule) {
                    wp_dequeue_script('contact-form-7');
                }
            });
        }

        if ($this->isOptionPresent('autoTop', 'contactForm7')) {
            add_filter('wpcf7_autop_or_not', '__return_false');
        }
    }

    public function getConfigName(): string
    {
        return 'frontCleaner';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->settings = $configArguments;
    }

    public function setHooks(): void
    {
        $this->cleanWp();
        $this->cleanYoast();
        $this->cleanWooCommerce();
        $this->cleanContactForm7();
    }
}
