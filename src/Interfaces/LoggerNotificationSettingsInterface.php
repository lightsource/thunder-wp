<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface LoggerNotificationSettingsInterface
{
    public function isLoggerNotificationEnabled():bool;

    public function getLoggerNotificationFromEmail(): string;

    public function getLoggerNotificationToEmails(): array;

    public function getLoggerNotificationMinLevelWeight(): int;
}
