<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface TemplatesInterface extends HooksInterface, ModuleInterface
{

}
