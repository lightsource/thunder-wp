<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface AcfBlockAssetsSettingsInterface
{
    public function getAssetsVersion(): string;

    public function isStaging(): bool;
}
