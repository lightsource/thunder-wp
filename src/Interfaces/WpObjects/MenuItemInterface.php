<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

use WP_Post;

interface MenuItemInterface
{
    public function getWpObject(): ?WP_Post;

    public function getParent(): ?MenuItemInterface;

    /**
     * @return MenuItemInterface[]
     */
    public function getChildren(): array;

    public function isActive(): bool;

    public function isChildActive(): bool;

    public function getUrl(): string;

    public function getTitle(): string;

    public function isBlank(): bool;

    public function getDeepClone(): self;

    public function setWpObject(?WP_Post $wpObject): void;

    public function setParent(?MenuItemInterface $parent): void;

    public function addChild(MenuItemInterface $item): void;
}
