<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

use LightSource\FrontBlocks\Interfaces\BlockInterface;

interface MenuItemBlockInterface extends BlockInterface
{
    public function loadByMenuItem(MenuItemInterface $menuItem): void;
}
