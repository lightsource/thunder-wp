<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

interface TaxonomyInterface extends CptDataInterface
{

}
