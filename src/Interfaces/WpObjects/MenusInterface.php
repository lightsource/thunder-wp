<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

use LightSource\ThunderWP\WpObjects\MenuData;

interface MenusInterface
{
    public function getMenu(string $menuSlug): MenuData;

    public function getMenuTwigArguments(
        string $menuSlug,
        MenuItemBlockInterface $firstLevel,
        MenuItemBlockInterface $secondLevel,
        MenuItemBlockInterface $thirdLevel
    ): array;
}
