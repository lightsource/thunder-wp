<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

interface MenuDataInterface
{
    public function addItem(MenuItemInterface $item): void;

    /**
     * @return MenuItemInterface[]
     */
    public function getItems(): array;

    public function setSlug(string $slug): void;

    public function getSlug(): string;

    public function getDeepClone(): self;
}
