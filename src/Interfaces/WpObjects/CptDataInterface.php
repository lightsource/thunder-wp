<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\WpObjects;

use LightSource\ThunderWP\Interfaces\HooksInterface;

interface CptDataInterface extends HooksInterface
{
    public function getSlug(): string;
}
