<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface TemplatesPageInterface
{
    public function getHeaderPostId(): int;

    public function getFooterPostId(): int;
}
