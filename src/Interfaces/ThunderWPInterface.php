<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

use Exception;

interface ThunderWPInterface
{
    /**
     * @param string $pathToConfig
     * @return void
     *
     * @throws Exception
     */
    public function loadFromFile(string $pathToConfig): void;

    /**
     * @param array $config
     * @return void
     *
     * @throws Exception
     */
    public function load(array $config): void;
}
