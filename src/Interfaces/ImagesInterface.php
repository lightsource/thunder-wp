<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface ImagesInterface extends HooksInterface, ModuleInterface
{
    const SIZE_THUMBNAIL = 'thumbnail';
    const SIZE_MEDIUM = 'medium';
    const SIZE_LARGE = 'large';
    const SIZE_FULL = 'full';

    public function getInlineSvg(string $file, string $classes = ''): string;

    public function getThemeInlineSvg(string $name, string $classes = ''): string;

    public function getAttachmentInlineSvg(int $imageId, string $classes = ''): string;

    public function getImage(
        int $imageId,
        string $size,
        string $classes = '',
        bool $isWithoutLazy = false,
        bool $isSkipWebp = false
    ): string;

    // use 'getImage()' wherever it's possible, this method e.g. for getting icons for emails
    public function getImageUrl(int $imageId, string $size, bool $isSkipWebp = false): string;
}
