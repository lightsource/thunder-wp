<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

use LightSource\FrontBlocks\Interfaces\BlockInterface;

interface TemplateBlockInterface extends BlockInterface
{
    public function loadByTemplate(string $template): void;
}
