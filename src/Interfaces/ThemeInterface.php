<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface ThemeInterface extends HooksInterface, ModuleInterface
{
    public function printHtmlHeader(): void;

    public function printHtmlFooter(): void;
}
