<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

use LightSource\FrontBlocks\Interfaces\BlockInterface;

interface WooBlockInterface extends BlockInterface
{
    public function loadByWooCommerce(string $template, array $arguments): void;
}
