<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface FrontCleanerInterface extends ModuleInterface, HooksInterface
{

}
