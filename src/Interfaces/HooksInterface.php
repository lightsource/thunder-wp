<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface HooksInterface
{
    public function setHooks():void;
}
