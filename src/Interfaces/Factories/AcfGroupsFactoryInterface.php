<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\Factories;

use LightSource\AcfBlocks\Interfaces\AcfBlocksInterface;
use LightSource\AcfGroups\Interfaces\LoaderInterface as AcfGroupsLoaderInterface;
use LightSource\ThunderWP\Interfaces\ModuleInterface;

interface AcfGroupsFactoryInterface extends FactoryInterface, ModuleInterface
{

}
