<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\Factories;

use LightSource\FrontBlocks\Interfaces\FrontBlocksInterface;
use LightSource\ThunderWP\Interfaces\ModuleInterface;

interface FrontBlocksFactoryInterface extends FactoryInterface, ModuleInterface
{
    public function makeInstance(): FrontBlocksInterface;
}
