<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\Factories;

use Exception;

interface FactoryInterface
{
    /**
     * @return mixed
     * @throws Exception
     */
    public function makeInstance();

    public function getContainerProperties($instance): array;
}
