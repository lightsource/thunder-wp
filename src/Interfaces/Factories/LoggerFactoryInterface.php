<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\Factories;

use LightSource\ThunderWP\Interfaces\ModuleInterface;

interface LoggerFactoryInterface extends FactoryInterface, ModuleInterface
{

}
