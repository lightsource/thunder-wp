<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces\Factories;

use LightSource\AcfBlocks\Interfaces\AcfBlocksInterface;
use LightSource\ThunderWP\Interfaces\ModuleInterface;

interface AcfBlocksFactoryInterface extends FactoryInterface, ModuleInterface
{

}