<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

interface AcfBlockAssetsPageInterface
{
    public function getCssCode(): string;
}
