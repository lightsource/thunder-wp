<?php


declare(strict_types=1);

namespace LightSource\ThunderWP\Interfaces;

use Exception;

interface ModuleInterface
{
    public function getConfigName(): string;

    /**
     * @param array $configArguments
     * @throws Exception
     */
    public function setConfigArguments(array $configArguments): void;
}
