<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Factories;

use LightSource\AcfBlocks\AcfBlocks;
use LightSource\AcfBlocks\Interfaces\AcfBlocksInterface;
use LightSource\AcfBlocks\Interfaces\SettingsInterface;
use LightSource\AcfBlocks\Settings;
use LightSource\FrontBlocks\Interfaces\FrontBlocksInterface;
use LightSource\ThunderWP\Interfaces\Factories\AcfBlocksFactoryInterface;

class AcfBlocksFactory implements AcfBlocksFactoryInterface
{
    private FrontBlocksInterface $frontBlocks;
    private string $pathToBlockPages;
    private string $urlOfBlockPages;
    private string $phpFileRegex;

    public function __construct(FrontBlocksInterface $frontBlocks)
    {
        $this->frontBlocks = $frontBlocks;
        $this->pathToBlockPages = '';
        $this->urlOfBlockPages = '';
        $this->phpFileRegex = '';
    }

    protected function getFrontBlocks(): FrontBlocksInterface
    {
        return $this->frontBlocks;
    }

    protected function getPathToBlockPages(): string
    {
        return $this->pathToBlockPages;
    }

    protected function getUrlOfBlockPages(): string
    {
        return $this->urlOfBlockPages;
    }

    protected function getPhpFileRegex(): string
    {
        return $this->phpFileRegex;
    }

    public function getInstanceType(): string
    {
        return AcfBlocksInterface::class;
    }

    /**
     * @return AcfBlocks
     */
    public function makeInstance()
    {
        $settings = new Settings();
        $settings->setPathToAssetsFolder($this->pathToBlockPages);
        $settings->setUrlOfAssetsFolder($this->urlOfBlockPages);
        $settings->setLateHeadFilter('thunder-wp__head');

        $acfBlocks = new AcfBlocks(
            $settings,
            $this->frontBlocks->getLoader(),
            $this->frontBlocks->getCreator(),
            $this->frontBlocks->getRenderer(),
            $this->frontBlocks->getResourceCreator()
        );

        $acfBlocks->setup($this->phpFileRegex);

        return $acfBlocks;
    }

    /**
     * @param AcfBlocks $instance
     * @return array
     */
    public function getContainerProperties($instance): array
    {
        return [
            AcfBlocksInterface::class => $instance,
            SettingsInterface::class => $instance->getSettings(),
        ];
    }

    public function getConfigName(): string
    {
        return 'acfBlocks';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->pathToBlockPages = $configArguments['pathToBlockPages'] ?? '';
        $this->urlOfBlockPages = $configArguments['urlOfBlockPages'] ?? '';
        $this->phpFileRegex = $configArguments['phpFileRegex'] ?? '';
    }
}
