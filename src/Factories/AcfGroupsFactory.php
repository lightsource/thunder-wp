<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Factories;

use Exception;
use LightSource\AcfGroups\Creator;
use LightSource\AcfGroups\Interfaces\CreatorInterface;
use LightSource\AcfGroups\Interfaces\LoaderInterface;

use LightSource\AcfGroups\Loader;
use LightSource\ThunderWP\Interfaces\Factories\AcfGroupsFactoryInterface;


class AcfGroupsFactory implements AcfGroupsFactoryInterface
{
    private array $paths;

    public function __construct()
    {
        $this->paths = [];
    }

    protected function getPaths(): array
    {
        return $this->paths;
    }

    /**
     * @return Loader
     * @throws Exception
     */
    public function makeInstance()
    {
        $acfGroupsLoader = new Loader();

        foreach ($this->paths as $path) {
            $acfGroupsLoader->signUpGroups(
                $path['namespace'] ?? '',
                $path['folder'] ?? '',
                $path['phpFileRegex'] ?? ''
            );
        }

        return $acfGroupsLoader;
    }

    public function getContainerProperties($instance): array
    {
        return [
            CreatorInterface::class => new Creator(),
            LoaderInterface::class => $instance,
        ];
    }

    public function getConfigName(): string
    {
        return 'acfGroups';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->paths = $configArguments['paths'] ?? '';
    }
}
