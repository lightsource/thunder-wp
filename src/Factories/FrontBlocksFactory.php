<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Factories;

use DI\Container;
use Exception;
use LightSource\FrontBlocks\FrontBlocks;
use LightSource\FrontBlocks\Interfaces\CreatorInterface;
use LightSource\FrontBlocks\Interfaces\FrontBlocksInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;
use LightSource\FrontBlocks\Interfaces\TemplateInterface;
use LightSource\FrontBlocks\Settings;
use LightSource\ThunderWP\Interfaces\Factories\FrontBlocksFactoryInterface;
use Psr\Log\LoggerInterface;
use Twig\TwigFunction;

class FrontBlocksFactory implements FrontBlocksFactoryInterface
{
    private LoggerInterface $logger;
    private Container $container;
    private array $paths;

    public function __construct(
        LoggerInterface $logger,
        Container $container
    ) {
        $this->logger = $logger;
        $this->container = $container;
        $this->paths = [];
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    protected function getContainer(): Container
    {
        return $this->container;
    }

    protected function getPaths(): array
    {
        return $this->paths;
    }

    /**
     * @param FrontBlocks $instance
     * @return array
     */
    public function getContainerProperties($instance): array
    {
        return [
            FrontBlocksInterface::class => $instance,
            RendererInterface::class => $instance->getRenderer(),
            CreatorInterface::class => $instance->getCreator(),
            TemplateInterface::class => $instance->getTemplate(),
        ];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function makeInstance(): FrontBlocksInterface
    {
        $settings = new Settings();

        foreach ($this->paths as $path) {
            $settings->addBlocksFolder(
                $path['namespace'] ?? '',
                $path['folder'] ?? '',
            );
        }

        $frontBlocks = new FrontBlocks($settings, $this->container, $this->logger);
        $frontBlocks->getTwig()->getEnvironment()->addFunction(
            new TwigFunction('_image', function ($image, $class = '') {
                if (!$class) {
                    return $image;
                }

                return str_replace('class="', sprintf('class="%s ', $class), $image);
            })
        );

        return $frontBlocks;
    }

    public function getConfigName(): string
    {
        return 'frontBlocks';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->paths = $configArguments['paths'] ?? '';
    }
}
