<?php

declare(strict_types=1);

namespace LightSource\ThunderWP\Factories;

use DI\Container;
use LightSource\Log\Log;
use LightSource\Log\Settings;
use LightSource\ThunderWP\Interfaces\Factories\LoggerFactoryInterface;
use LightSource\ThunderWP\Interfaces\LoggerNotificationSettingsInterface;
use Psr\Log\LoggerInterface;

class LoggerFactory implements LoggerFactoryInterface
{
    private ?LoggerNotificationSettingsInterface $notificationSettings;
    private Container $container;
    private string $folder;
    private string $notificationSettingsClass;

    public function __construct(Container $container)
    {
        $this->folder = '';
        $this->notificationSettings = null;
        $this->container = $container;
        $this->notificationSettingsClass = '';
    }

    protected function getLoNotificationSettings(): ?LoggerNotificationSettingsInterface
    {
        return $this->notificationSettings;
    }

    protected function getNotificationSettingsClass(): string
    {
        return $this->notificationSettingsClass;
    }

    protected function getContainer(): Container
    {
        return $this->container;
    }

    protected function getFolder(): string
    {
        return $this->folder;
    }

    protected function setNotification(Settings $settings): void
    {
        // late initialization, as LoggerFactory is the first in the 'instances' list,
        // so if we init right in the 'setConfigArguments()' method, then nothing is defined yet
        // real example: it's most likely an Acf Group, so 'LightSource\AcfGroups\Interfaces\CreatorInterface' won't be defined yet
        $this->notificationSettings = $this->container->get($this->notificationSettingsClass);

        if (!$this->notificationSettings->isLoggerNotificationEnabled()) {
            return;
        }

        $settings->setNotificationMinLevel(
            $this->notificationSettings->getLoggerNotificationMinLevelWeight()
        );
        $settings->setNotificationCallback([$this, 'notifyAboutNewLogRecords']);
    }

    public function getContainerProperties($instance): array
    {
        return [
            LoggerInterface::class => $instance,
        ];
    }

    public function makeInstance(): LoggerInterface
    {
        if (!is_dir($this->folder)) {
            mkdir($this->folder);
        }

        $settings = new Settings();
        $settings->setPathToLogDir($this->folder);

        if ($this->notificationSettingsClass) {
            // get_field will be available only since this hook
            add_action('acf/init', function () use ($settings) {
                $this->setNotification($settings);
            }, 20);
        }

        return new Log($settings);
    }

    public function getConfigName(): string
    {
        return 'logger';
    }

    public function setConfigArguments(array $configArguments): void
    {
        $this->folder = $configArguments['folder'] ?? '';
        $this->notificationSettingsClass = $configArguments['notificationSettings'] ?? '';
    }

    public function notifyAboutNewLogRecords(string $level): void
    {
        $toEmails = $this->notificationSettings->getLoggerNotificationToEmails();
        $fromEmail = $this->notificationSettings->getLoggerNotificationFromEmail();

        foreach ($toEmails as $toEmail) {
            $subject = sprintf('New "%s" in the log file', $level);
            $message = sprintf('New "%s" record appeared in the log file. Website: %s', $level, get_site_url());

            wp_mail($toEmail, $subject, $message, [
                'Content-Type: text/html; charset=UTF-8',
                sprintf('From: Website <%s>', $fromEmail),
            ]);
        }
    }
}
